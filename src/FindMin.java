public class FindMin {

    public static void main(String[] args) {
        int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int value3 = Integer.parseInt(args[2]);
        int result ;

        boolean someCondition1 = value1<=value2;
        boolean someCondition2 = value1<=value3;

        if (someCondition1 == true && someCondition2 == true) {
            result = value1;
        } else if (someCondition1 == true && someCondition2 == false) {
            result = value3;
        } else if (someCondition1 == false && someCondition2 == true) {
            result = value2;
        } else {
            if (value2 >= value3) {
                result = value3;
            } else {
                result = value2;
            }
        }

        System.out.println(result);

    }
}